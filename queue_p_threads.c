#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h> 

#define QUEUESIZE 16
#define LOOP 100000

#define PRODUCERS 8
#define CONSUMERS 16
#define TIMES 2000


void *producer (void *args);
void *consumer (void *args);

struct timeval start_time, end_time;
long usec[1000000];
long times = 0;
pthread_cond_t *desired_times;
pthread_mutex_t *times_mutex;


///////////////
typedef struct{ 
	void * (*work)(void *arg); 
	void *arg; 
}workFunctionData;
///////////////



typedef struct {
  // Buf: array with size: QUEUESIZE
  workFunctionData buf[QUEUESIZE];
  // head and tail are indexes for the array
  long head, tail;
  // full and empty are used as flags // 1 or 0 // TRUE or FALSE
  int full, empty;
  // mutex variable for controlling thread access to data
  pthread_mutex_t *mut; 
  // condition variables to allow threads to synchronize based upon the actual value of data.
  pthread_cond_t *notFull, *notEmpty;
} queue;

queue *queueInit (void);
void queueDelete (queue *q);
// MODIFICATION //
void queueAdd (queue *q, workFunctionData in);
void queueDel (queue *q, workFunctionData *out);

// MODIFICATION //


/////////////////////////////////////////////////////
void *workTodo(void *){
  printf("i am working now");
  return NULL;
}
////////////////////////////////////////////////////


void *measure_time(void *arg){
  
    while(times < TIMES) {
        pthread_cond_wait(desired_times, times_mutex);
    }
    long local_times = times;
    FILE *fp = fopen("time_measurements.txt","wb");
    for(long i = 0; i < local_times; i++)
    {
      char str[32];
      sprintf(str, "%ld", usec[i]);               // Convert the time into string to save it on the file
      fputs(str, fp);                            // Place the string(time) into the file
      fputc('\n',fp);
    }
    fclose(fp);   
  
  return NULL;
}


int main ()
{

  // MODIFICATION // 
  // Create an array of threads //
  pthread_t pro[PRODUCERS], con[CONSUMERS];
  pthread_t time;

  times_mutex = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
  desired_times = (pthread_cond_t *) malloc(sizeof(pthread_cond_t));
   
  pthread_cond_init(desired_times, NULL);
  pthread_mutex_init(times_mutex, NULL);
   // MODIFICATION // 

  // Create a pointer of type queue and perform the initialization of this queue 
  queue *fifo;
  fifo = queueInit ();
  // if the queueInit don't run properly an error message //
  if (fifo ==  NULL) {
    fprintf (stderr, "main: Queue Init failed.\n");
    exit (1);
  }
  
  // Create the threads to start to run the routines producer and consumer //
   

  for(int i = 0; i < CONSUMERS; i++)
    pthread_create(&con[i], NULL, consumer, fifo);

  for(int i = 0; i < PRODUCERS; i++)
    pthread_create(&pro[i], NULL, producer, fifo);
  
  pthread_create(&time, NULL, measure_time, NULL);
  
  for(int i = 0; i < PRODUCERS; i++)
    pthread_join(pro[i], NULL);
  
  for(int i = 0; i < CONSUMERS; i++)
    pthread_join(con[i], NULL);
  // create a thread that stores the times

  pthread_join(time, NULL);
  // delete the fifo 
  queueDelete (fifo);
  
  pthread_cond_destroy(desired_times);
  pthread_mutex_destroy(times_mutex);

  free(desired_times);
  free(times_mutex);

  return 0;
}

void *producer (void *q)
{
  /////////////////////////////////////////////////////
  workFunctionData in;
 
  queue *fifo;
  fifo = (queue *)q;
  
  int i;
  for (i = 0; i < LOOP; i++) {
    pthread_mutex_lock (fifo->mut);
    // fifo->full is the condition
    while (fifo->full) {
      printf ("producer: queue FULL.\n");
      pthread_cond_wait(fifo->notFull, fifo->mut);
    }
    ///////////////////////////////
    in.arg = NULL;
    in.work = workTodo;
    queueAdd (fifo, in);
    //////////////////////////////
    pthread_mutex_unlock (fifo->mut);
    pthread_cond_signal (fifo->notEmpty);
  }
  return (NULL);
}

void *consumer (void *q)
{
  queue *fifo;
  fifo = (queue *)q;
  //////////////////////////////////////
  workFunctionData out;
  //////////////////////////////////////
  while(1)
  {
    pthread_mutex_lock (fifo->mut);
    while (fifo->empty) {
      printf ("consumer: queue EMPTY.\n");
      pthread_cond_wait (fifo->notEmpty, fifo->mut);
    }
    //////////////////////////////
    queueDel (fifo, &out);                              // The out workFunction will take the final workFunction queue element
    out.work(out.arg);                                  // Here the consumer calls the work function
    free(out.arg);
    /////////////////////////////
    pthread_mutex_unlock (fifo->mut);
    pthread_cond_signal (fifo->notFull);                // The producer thread waits for the notFull signal
    printf("consumer: recieved");
    printf("\n");
  }

  return (NULL);
}


queue *queueInit (void)
{
  // pointer of type queue to points to the data of struct queue
  queue *q;

  // allocate space for the queue
  q = (queue *)malloc (sizeof (queue));
  if (q == NULL) return (NULL);

  // q is empty = true
  q->empty = 1;
  // q is full = false
  q->full = 0;
  // head and tail of the queue are pointing in the first position of the array
  q->head = 0;
  q->tail = 0;
  // allocate space for the mutex var
  q->mut = (pthread_mutex_t *) malloc (sizeof (pthread_mutex_t));
  // init mutex
  pthread_mutex_init (q->mut, NULL);
  // allocate space for the notFull condition var
  q->notFull = (pthread_cond_t *) malloc (sizeof (pthread_cond_t));
  // init the notFull condition var
  pthread_cond_init (q->notFull, NULL);
  // allocate space for the notEmpty condition var
  q->notEmpty = (pthread_cond_t *) malloc (sizeof (pthread_cond_t));
  // init the notEmpty condition var
  pthread_cond_init (q->notEmpty, NULL);
	
  // returns a pointer that points in the first pos of the array/queue
  return (q);
}

void queueDelete (queue *q)
{
  // Destroy the mutex var
  pthread_mutex_destroy (q->mut);
  // free the mutex's space
  free (q->mut);	
  // Destroy the notFull condition var
  pthread_cond_destroy (q->notFull);
  // free the notFull's space
  free (q->notFull);
  // Destroy the notEmpty condition var
  pthread_cond_destroy (q->notEmpty);
  // free the notEmpty's space
  free (q->notEmpty);
  // free the q array space
  free (q);
}

// MODIFICATION //
// In the queue must be added types of workfunction not int //

void queueAdd (queue *q, workFunctionData in)
{
  // place the in value in the tail index of the array
  q->buf[q->tail] = in;
  // capture the time
  gettimeofday(&start_time, NULL);
  // increase the tail index
  q->tail++;
  // If the tail went at the final element of the queue, place the tail at the start // why ???
  if (q->tail == QUEUESIZE)
    q->tail = 0;
  
  if (q->tail == q->head)
    q->full = 1;
  q->empty = 0;
  return;
}

// MODIFICATION //
// In the queue must be deleted types of workfunction not int //

void queueDel (queue *q, workFunctionData *out)
{
  
  *out = q->buf[q->head];
  //////////////////////////////////////////////////////
  // Capture the end time //
  gettimeofday(&end_time, NULL);
  usec[times] = end_time.tv_usec - start_time.tv_usec;
  //////////////////////////
  printf("number of measurement = %ld :", times);
  printf(" time = %ld", usec[times]);
  if(times == TIMES)
    pthread_cond_signal(desired_times);
  times += 1;
  printf("\n");
  //////////////////////////////////////////////////////
  q->head++;
  if (q->head == QUEUESIZE)
    q->head = 0;
  if (q->head == q->tail)
    q->empty = 1;
  q->full = 0;
  return;
}